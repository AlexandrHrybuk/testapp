package com.example.hrybuk.testapplication.screens.splash.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hrybuk.testapplication.R;
import com.example.hrybuk.testapplication.screens.splash.presenters.SplashFragmentPresenter;
import com.example.hrybuk.testapplication.screens.main.views.MainActivityView;
import com.example.hrybuk.testapplication.screens.productslist.views.ProductsFragmentView;

/**
 * Created by hrybuk on 19.04.2017.
 */

public class SplashFragmentView extends Fragment implements ISplashFragmentView {

    private SplashFragmentPresenter presenter;

    public static SplashFragmentView getInstance() {
        return new SplashFragmentView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        init();
        presenter.startCountdown();
        return inflater.inflate(R.layout.splash_fragment_layout, container, false);
    }

    private void init() {
        presenter = new SplashFragmentPresenter(this);
        ((MainActivityView) getActivity()).setToolbarVisibility(false);
    }

    @Override
    public void closeSplash() {
        ((MainActivityView) getActivity()).setToolbarVisibility(true);
        ((MainActivityView) getActivity()).changeRootFragment(ProductsFragmentView.getInstance());
    }
}
