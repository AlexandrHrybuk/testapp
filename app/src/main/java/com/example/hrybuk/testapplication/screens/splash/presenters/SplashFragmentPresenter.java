package com.example.hrybuk.testapplication.screens.splash.presenters;

import android.os.Handler;

import com.example.hrybuk.testapplication.screens.splash.views.ISplashFragmentView;

/**
 * Created by hrybuk on 19.04.2017.
 */

public class SplashFragmentPresenter {

    private ISplashFragmentView view;
    private final long SPLASH_DELAY = 3000;

    public SplashFragmentPresenter(ISplashFragmentView view) {
        this.view = view;
    }

    public void startCountdown() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view.closeSplash();
            }
        }, SPLASH_DELAY);
    }
}
