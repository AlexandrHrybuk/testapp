package com.example.hrybuk.testapplication.util;

/**
 * Created by hrybuk on 19.04.2017.
 */

public class Constants {
    public enum Keys {
        /* Keys constants */
        PRODUCT_KEY("product_key");

        private final String key;

        private Keys(final String key) {
            this.key = key;
        }

        @Override
        public String toString() {
            return key;
        }
    }

    public enum ServerSide {
        /* Server url */
        SERVER_URL("http://google.com");

        private final String serverSide;

        private ServerSide(final String serverSide) {
            this.serverSide = serverSide;
        }

        @Override
        public String toString() {
            return serverSide;
        }
    }

}


