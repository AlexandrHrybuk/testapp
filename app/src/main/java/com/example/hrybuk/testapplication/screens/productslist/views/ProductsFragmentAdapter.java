package com.example.hrybuk.testapplication.screens.productslist.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hrybuk.testapplication.R;
import com.example.hrybuk.testapplication.database.ProductItemDb;
import com.example.hrybuk.testapplication.screens.main.views.MainActivityView;
import com.example.hrybuk.testapplication.screens.productdetail.views.ProductDetailFragmentView;
import com.example.hrybuk.testapplication.screens.productslist.models.ProductsFilter;
import com.example.hrybuk.testapplication.screens.productslist.presenters.ProductsFragmentPresenter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hrybuk on 22.04.2017.
 */
public class ProductsFragmentAdapter extends RecyclerView.Adapter<ProductsFragmentAdapter.ProductItemViewHolder> {

    private List<ProductItemDb> allItems, visibleItems;
    private Context context;
    private RecyclerView recyclerView;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;

    public ProductsFragmentAdapter(List<ProductItemDb> allItems, Context context, RecyclerView recyclerView) {
        this.allItems = allItems;
        this.context = context;
        this.recyclerView = recyclerView;
        this.recyclerView.addOnScrollListener(scrollListener);
        visibleItems = new ArrayList<>();
        int i = allItems.size() < 15 ? allItems.size() : 15;
        for (int j = 0; j < i; j++) {
            visibleItems.add(allItems.get(j));
        }

    }

    @Override
    public ProductItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.products_item_layout, parent, false);
        return new ProductItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductItemViewHolder holder, int position) {
        ProductItemDb item = visibleItems.get(position);
        if (item.iconUrl != null && item.iconUrl.length() > 0) {
            Picasso.with(context)
                    .load(item.iconUrl)
                    .into(holder.icon);
        }
        holder.title.setText(item.title);
        holder.description.setText(item.description);
    }

    @Override
    public int getItemCount() {
        return visibleItems.size();
    }

    public Filter getFilter(ProductsFragmentPresenter presenter, List<ProductItemDb> allItems) {
        return new ProductsFilter(presenter, allItems);
    }

    class ProductItemViewHolder extends RecyclerView.ViewHolder {
        private TextView title, description;
        private ImageView icon;

        public ProductItemViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.products_item_title);
            description = (TextView) itemView.findViewById(R.id.products_item_description);
            icon = (ImageView) itemView.findViewById(R.id.products_item_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int itemPosition = recyclerView.getChildLayoutPosition(view);
                    ((MainActivityView) context).addFragment(ProductDetailFragmentView.getInstance(visibleItems.get(itemPosition).getId()));
                }
            });
        }
    }

    RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) {
                visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                totalItemCount = recyclerView.getLayoutManager().getItemCount();
                pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    int i = (visibleItems.size() + 15) > allItems.size() ? allItems.size() - visibleItems.size() : 15;
                    int j = visibleItems.size();
                    for (; i > 0; i--) {
                        visibleItems.add(allItems.get(j));
                        j++;
                    }
                    notifyDataSetChanged();
                }
            }
        }
    };
}
