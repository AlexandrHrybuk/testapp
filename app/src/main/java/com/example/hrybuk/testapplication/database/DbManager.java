package com.example.hrybuk.testapplication.database;

import com.activeandroid.Model;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by hrybuk on 21.04.2017.
 */

public class DbManager {
    // get data from db method
    public <T extends Model> List<T> loadData(Class<? extends Model> model) {
        return new Select().from(model).execute();
    }

}
