package com.example.hrybuk.testapplication.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

/**
 * Created by hrybuk on 20.04.2017.
 */

//db model
@Table(name = "ProductItemTable")
public class ProductItemDb extends Model {

    @Expose
    @Column(name = "productIconCreated")
    public long created;

    @Expose
    @Column(name = "productIconUrl")
    public String iconUrl;

    @Expose
    @Column(name = "productIconTitle")
    public String title;

    @Expose
    @Column(name = "productIconDescription")
    public String description;

    public ProductItemDb() {

    }

}
