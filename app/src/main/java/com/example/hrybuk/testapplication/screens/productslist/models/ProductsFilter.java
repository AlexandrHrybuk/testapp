package com.example.hrybuk.testapplication.screens.productslist.models;

import android.widget.Filter;

import com.example.hrybuk.testapplication.database.ProductItemDb;
import com.example.hrybuk.testapplication.screens.productslist.presenters.ProductsFragmentPresenter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by hrybuk on 22.04.2017.
 */

//search filter
public class ProductsFilter extends Filter {

    private final ProductsFragmentPresenter presenter;
    private final List<ProductItemDb> originalList;
    private final List<ProductItemDb> filteredList;

    public ProductsFilter(ProductsFragmentPresenter presenter, List<ProductItemDb> originalList) {
        super();
        this.presenter = presenter;
        this.originalList = new LinkedList<>(originalList);
        this.filteredList = new ArrayList<>();
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        filteredList.clear();
        final FilterResults results = new FilterResults();

        if (constraint.length() == 0) {
            filteredList.addAll(originalList);
        } else {
            final String filterPattern = constraint.toString();

            for (final ProductItemDb item : originalList) {
                if (containsIgnoreCase(item.title, filterPattern)) {
                    filteredList.add(item);
                }
            }
        }
        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        presenter.searchResult(filteredList);
    }

    private boolean containsIgnoreCase(String str, String searchStr) {
        if (str == null || searchStr == null) return false;

        final int length = searchStr.length();
        if (length == 0)
            return true;

        for (int i = str.length() - length; i >= 0; i--) {
            if (str.regionMatches(true, i, searchStr, 0, length))
                return true;
        }
        return false;
    }
}
