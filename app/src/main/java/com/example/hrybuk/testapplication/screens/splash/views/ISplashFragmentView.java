package com.example.hrybuk.testapplication.screens.splash.views;

/**
 * Created by hrybuk on 21.04.2017.
 */

public interface ISplashFragmentView {

    void closeSplash();

}
