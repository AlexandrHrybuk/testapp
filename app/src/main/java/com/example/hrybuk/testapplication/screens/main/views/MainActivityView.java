package com.example.hrybuk.testapplication.screens.main.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.hrybuk.testapplication.R;
import com.example.hrybuk.testapplication.screens.splash.views.SplashFragmentView;


/**
 * Created by hrybuk on 18.04.2017.
 */

public class MainActivityView extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    private Toolbar toolbar;
    private FragmentManager supportFragmentManager;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);
        supportFragmentManager = getSupportFragmentManager();
        supportFragmentManager.addOnBackStackChangedListener(this);
        initToolbar();
        //run start fragment
        changeRootFragment(SplashFragmentView.getInstance());
    }

    //    toolbar settings
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        searchView = (SearchView) toolbar.findViewById(R.id.toolbar_searchview);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void setToolbarVisibility(boolean isVisible) {
        toolbar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    //    fragment navigation
    public void shouldDisplayHomeOrSearch() {
        boolean isNeedBack = getSupportFragmentManager().getBackStackEntryCount() > 1;
        getSupportActionBar().setDisplayHomeAsUpEnabled(isNeedBack);
        searchView.setVisibility(!isNeedBack ? View.VISIBLE : View.GONE);

    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    public void addFragment(Fragment fragment) {
        supportFragmentManager.beginTransaction().add(R.id.fragment_container, fragment).addToBackStack(null).commit();
    }

    public void changeRootFragment(Fragment fragment) {
        //added for possible future navigation
        for (int i = 0; i < supportFragmentManager.getBackStackEntryCount(); ++i) {
            supportFragmentManager.popBackStack();
        }
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        supportFragmentManager.popBackStack();
        if (supportFragmentManager.getBackStackEntryCount() == 1) {
            super.onBackPressed();
        }
    }

    //    other

    public void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    public SearchView getSearchView() {
        return searchView;
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeOrSearch();
    }
}
