package com.example.hrybuk.testapplication.network.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;

import com.example.hrybuk.testapplication.R;
import com.example.hrybuk.testapplication.database.DbManager;
import com.example.hrybuk.testapplication.network.NetworkHelper;
import com.example.hrybuk.testapplication.database.ProductItemDb;
import com.example.hrybuk.testapplication.network.interfaces.IDataLoadedCallback;

import java.util.List;

/**
 * Created by hrybuk on 20.04.2017.
 */

public class GetProductsTask extends AsyncTask<Void, Void, Boolean> {

    private IDataLoadedCallback iDataLoadedCallback;
    private Context context;
    private List<ProductItemDb> items;

    public GetProductsTask(IDataLoadedCallback iDataLoadedCallback, Context context) {
        this.iDataLoadedCallback = iDataLoadedCallback;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        //uncomment this when server will be ready

//        IGetProductsApi api = ServiceBuilder.createService(IGetProductsApi.class);
//        try {
//            items = api.getProducts().execute().body();
//            return true;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }

        if (NetworkHelper.isNetworkAvailable(context)) {
            //request and response imitation
            ProductItemDb newItem = new ProductItemDb();
            newItem.created = System.currentTimeMillis();
            DbManager dbManager = new DbManager();
            newItem.title = context.getString(R.string.product_title) + " " + dbManager.loadData(ProductItemDb.class).size();
            newItem.description = context.getString(R.string.product_description);
            newItem.save();
            items = dbManager.loadData(ProductItemDb.class); //here must be items from server
            SystemClock.sleep(3000);
            return true;
        } else {
            //load from cash imitation
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (aBoolean) {
            iDataLoadedCallback.onSuccess(items);
        } else {
            iDataLoadedCallback.onFailure();
        }
    }
}
