package com.example.hrybuk.testapplication.screens.productslist.views;

import com.example.hrybuk.testapplication.database.ProductItemDb;

import java.util.List;

/**
 * Created by hrybuk on 21.04.2017.
 */

public interface IProductsFragmentView {

    void fillList(List<ProductItemDb> products);

    void clearSearchQuery();

}
