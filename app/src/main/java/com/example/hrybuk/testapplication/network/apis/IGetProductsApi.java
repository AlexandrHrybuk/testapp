package com.example.hrybuk.testapplication.network.apis;

import com.example.hrybuk.testapplication.database.ProductItemDb;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by hrybuk on 20.04.2017.
 */
//api for retrofit
public interface IGetProductsApi {
    @GET("/getProducts")
    Call<List<ProductItemDb>> getProducts();
}
