package com.example.hrybuk.testapplication.screens.productslist.presenters;

import android.content.Context;
import android.os.AsyncTask;

import com.example.hrybuk.testapplication.R;
import com.example.hrybuk.testapplication.database.DbManager;
import com.example.hrybuk.testapplication.database.ProductItemDb;
import com.example.hrybuk.testapplication.screens.main.views.MainActivityView;
import com.example.hrybuk.testapplication.network.interfaces.IDataLoadedCallback;
import com.example.hrybuk.testapplication.network.tasks.GetProductsTask;
import com.example.hrybuk.testapplication.screens.productslist.views.IProductsFragmentView;
import com.example.hrybuk.testapplication.screens.productslist.views.ProductsFragmentAdapter;

import java.util.List;

/**
 * Created by hrybuk on 20.04.2017.
 */

public class ProductsFragmentPresenter implements IDataLoadedCallback {

    private IProductsFragmentView view;
    private Context context;
    private List<ProductItemDb> productItems;

    public ProductsFragmentPresenter(IProductsFragmentView view, Context context) {
        this.view = view;
        this.context = context;
    }

    //      network methods
    public void loadProducts() {
        new GetProductsTask(this, context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    //      getters
    public void getProducts() {
        view.fillList(productItems);
    }

    //    search methods

    public void filterList(String query, ProductsFragmentAdapter adapter) {
        if (adapter != null) {
            adapter.getFilter(this, productItems).filter(query);
        }
    }

    public void searchResult(List<ProductItemDb> searchProducts) {
        view.fillList(searchProducts);
    }

    //      network callback
    @Override
    public void onSuccess(Object object) {
        productItems = (List<ProductItemDb>) object;
        view.clearSearchQuery();
        getProducts();
    }

    @Override
    public void onFailure() {
        ((MainActivityView) context).showToast(context.getString(R.string.network_error));
        DbManager dbManager = new DbManager();
        productItems = dbManager.loadData(ProductItemDb.class);
        if (productItems.size() == 0) {
            ((MainActivityView) context).showToast(context.getString(R.string.cash_error));
        } else {
            ((MainActivityView) context).showToast(context.getString(R.string.network_error));
        }
        view.clearSearchQuery();
        getProducts();
    }
}
