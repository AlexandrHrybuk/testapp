package com.example.hrybuk.testapplication.network.interfaces;


/**
 * Created by hrybuk on 20.04.2017.
 */
//network callback
public interface IDataLoadedCallback {

    void onSuccess(Object object);

    void onFailure();
}
