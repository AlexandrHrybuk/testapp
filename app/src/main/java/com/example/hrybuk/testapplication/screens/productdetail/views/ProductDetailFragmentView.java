package com.example.hrybuk.testapplication.screens.productdetail.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hrybuk.testapplication.R;
import com.example.hrybuk.testapplication.database.ProductItemDb;
import com.example.hrybuk.testapplication.util.Constants;
import com.squareup.picasso.Picasso;

/**
 * Created by hrybuk on 20.04.2017.
 */

public class ProductDetailFragmentView extends Fragment {

    private View rootView;
    private ProductItemDb productItem;

    public static ProductDetailFragmentView getInstance(long productId) {
        ProductDetailFragmentView fragment = new ProductDetailFragmentView();
        Bundle args = new Bundle();
        args.putLong(Constants.Keys.PRODUCT_KEY.toString(), productId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.product_info_fragment_layout, container, false);
        init();
        return rootView;
    }

    private void init() {
        productItem = ProductItemDb.load(ProductItemDb.class, getArguments().getLong(Constants.Keys.PRODUCT_KEY.toString()));
        if (productItem != null) {
            if (productItem.iconUrl != null && productItem.iconUrl.length() > 0) {
                Picasso.with(getContext())
                        .load(productItem.iconUrl)
                        .into((ImageView) rootView.findViewById(R.id.products_item_image));
            }
            ((TextView) rootView.findViewById(R.id.products_item_title)).setText(productItem.title);
            ((TextView) rootView.findViewById(R.id.products_item_description)).setText(productItem.description);
        }
    }
}
