package com.example.hrybuk.testapplication.screens.productslist.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hrybuk.testapplication.R;
import com.example.hrybuk.testapplication.database.ProductItemDb;
import com.example.hrybuk.testapplication.screens.main.views.MainActivityView;
import com.example.hrybuk.testapplication.screens.productslist.presenters.ProductsFragmentPresenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by hrybuk on 19.04.2017.
 */

public class ProductsFragmentView extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        IProductsFragmentView, SearchView.OnQueryTextListener {

    private SwipeRefreshLayout rootView;
    private ProductsFragmentPresenter presenter;
    private List<ProductItemDb> productItems;
    private RecyclerView productsList;
    private ProductsFragmentAdapter adapter;
    private SearchView searchView;

    public static ProductsFragmentView getInstance() {
        return new ProductsFragmentView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (SwipeRefreshLayout) inflater.inflate(R.layout.products_fragment_layout, container, false);
        init();
        return rootView;
    }

    private void init() {
        presenter = new ProductsFragmentPresenter(this, getActivity());
        rootView.setOnRefreshListener(this);
        rootView.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.refresh_blue),
                ContextCompat.getColor(getContext(), R.color.refresh_red));
        productsList = (RecyclerView) rootView.findViewById(R.id.products_list);
        productsList.setLayoutManager(new LinearLayoutManager(getContext()));
        productItems = new ArrayList<>();
        searchView = ((MainActivityView) getActivity()).getSearchView();
        searchView.setOnQueryTextListener(this);
        getProducts();
    }

    //getters
    private void getProducts() {
        rootView.setRefreshing(true);
        presenter.loadProducts();
    }

    //list methods
    @Override
    public void fillList(List<ProductItemDb> products) {
        rootView.setRefreshing(false);
        productItems = products;
        sortList(productItems);
        adapter = new ProductsFragmentAdapter(products, getContext(), productsList);
        productsList.setAdapter(adapter);
    }

    private void sortList(List<ProductItemDb> items) {
        Collections.sort(items, new Comparator<ProductItemDb>() {
            @Override
            public int compare(ProductItemDb productItemDb, ProductItemDb t1) {
                Long a = productItemDb.created;
                return a.compareTo(t1.created);
            }
        });
        Collections.reverse(items);
    }

    //search methods
    @Override
    public void clearSearchQuery() {
        searchView.setQuery("", false);
        searchView.clearFocus();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        presenter.filterList(newText, adapter);
        return true;
    }

    //swipe layout listener
    @Override
    public void onRefresh() {
        getProducts();
    }
}
